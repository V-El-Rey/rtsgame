using Abstractions;
using UnityEngine;
using Utils.AssetInjector;

namespace UserControlSystem
{
    public class ProduceUnitCommand : IProduceUnitCommand
    {
        public GameObject UnitPrefab => _unitPrefab;
        [InjectAsset("Chomper")]private GameObject _unitPrefab;
    }
}
