using System;
using Abstractions;
using UserControlSystem;
using UserControlSystem.UI.Model.CommandCreators;
using Utils.AssetInjector;
using Zenject;

public class AttackCommandCommandCreator : CommandCreatorBase<IAttackCommand>
{
    [Inject] private AssetsContext _context;
    protected override void СlassSpecificCommandCreation(Action<IAttackCommand> creationCallback)
    {
        creationCallback?.Invoke(_context.Inject(new AttackCommand()));
    }
}
