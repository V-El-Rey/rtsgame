using System;
using Abstractions;
using UnityEngine;
using Utils.AssetInjector;
using Zenject;

namespace UserControlSystem.UI.Model.CommandCreators
{
    public class StopCommandCommandCreator : CommandCreatorBase<IStopCommand>
    {
        [Inject] private AssetsContext _context;

        protected override void СlassSpecificCommandCreation(Action<IStopCommand> creationCallback)
        {
            creationCallback?.Invoke(_context.Inject(new StopCommand()));
        }
    }
}
