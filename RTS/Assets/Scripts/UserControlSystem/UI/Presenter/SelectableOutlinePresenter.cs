using Abstractions;
using UnityEngine;
using UserControlSystem.UI.Model;

namespace UserControlSystem.UI.Presenter
{
    public class SelectableOutlinePresenter : MonoBehaviour
    {
        [SerializeField] private SelectableValue _selectedValue;
        [SerializeField] private Material _outlineMaterial;
        private ISelectable currentSelectedObject;

        private void Start()
        {
            _selectedValue.OnSelected += OnSelected;
            OnSelected(_selectedValue.CurrentValue);
        }

        private void OnSelected(ISelectable selected)
        {
            if (selected != null)
            {
                selected.Renderer.material = _outlineMaterial;
                currentSelectedObject = selected;
            }
            else
            {
                if (currentSelectedObject != null)
                {
                    currentSelectedObject.Renderer.material = null;
                }
            }
        }
    }
}
