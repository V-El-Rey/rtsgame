using Abstractions;
using UnityEngine;

namespace Core
{
    public class Unit : MonoBehaviour, ISelectable
    {
        public float Health => _health;
        public float MaxHealth => _maxHealth;
        public Sprite Icon => _icon;
        public Renderer Renderer => _renderer;
        
        [SerializeField] private float _maxHealth = 200;
        [SerializeField] private Sprite _icon;
        [SerializeField] private Renderer _renderer;

        private float _health = 200;
    }
}
